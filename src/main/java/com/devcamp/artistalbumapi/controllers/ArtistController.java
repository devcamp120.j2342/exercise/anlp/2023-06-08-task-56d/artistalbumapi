package com.devcamp.artistalbumapi.controllers;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbumapi.models.Artist;
import com.devcamp.artistalbumapi.services.ArtistService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ArtistController {
    @Autowired
    ArtistService artistService;

    @GetMapping("/artists")
    public ArrayList<Artist> getAllArtist(){
        ArrayList<Artist> allArrtist = artistService.getAllArtist();
        return allArrtist;
    }

    @GetMapping("/artist-info")
    public Artist getArtistInfo(@RequestParam(name="code", required = true) int id){
        Artist findArtist = artistService.getArtistInfo(id);

        return findArtist;
    }

    @GetMapping("/artists/{index}")
    public Artist getArtistByIndex(@PathVariable() int index) {
        Artist indexArtist = artistService.getArtistByIndex(index);

        return indexArtist;
    }
}
