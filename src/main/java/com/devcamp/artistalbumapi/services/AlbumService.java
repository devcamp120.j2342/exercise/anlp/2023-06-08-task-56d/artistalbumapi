package com.devcamp.artistalbumapi.services;
import java.util.ArrayList;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.artistalbumapi.models.Album;

@Service
public class AlbumService {
    // hàm tạo arr list song cho album
    public ArrayList<String> createSongs() {
        ArrayList<String> arrSong = new ArrayList<>();
        arrSong.add("bai hat 1");
        arrSong.add("bai hat 2");
        arrSong.add("bai hat 3");
        return arrSong;
    }

    Album myWorld = new Album(1, "My World 2.0", createSongs());
    Album purpose = new Album(2, "Purpose", createSongs());
    Album Changes = new Album(3, "Changes", createSongs());

    Album StartBoy = new Album(1, "StartBoy", createSongs());
    Album dawnFM = new Album(2, "Dawn FM", createSongs());
    Album afterHours = new Album(3, "After Hours", createSongs());

    Album four = new Album(1, "Four", createSongs());
    Album takeMeHome = new Album(2, "TTake me home", createSongs());
    Album upAllNight = new Album(3, "Up All Night", createSongs());

    Album ngot = new Album(1, "Ngot", createSongs());
    Album dot = new Album(2, "Đot", createSongs());
    Album thayChua = new Album(3, "Thay Chua", createSongs());

    Album album2004 = new Album(1, "2004", createSongs());
    Album album5AM = new Album(2, "5AM", createSongs());
    Album gap = new Album(3, "Gap", createSongs());

    Album album25 = new Album(1, "2 5", createSongs());
    Album diathan = new Album(2, "Dia than", createSongs());
    Album redRum = new Album(3, "Red Rum", createSongs());

    public ArrayList<Album> getAlbum1(){
        ArrayList<Album> album1 = new ArrayList<>();

        album1.add(myWorld);
        album1.add(purpose);
        album1.add(Changes);

        return album1;
    }

    public ArrayList<Album> getAlbum2(){
        ArrayList<Album> album2 = new ArrayList<>();

        album2.add(StartBoy);
        album2.add(dawnFM);
        album2.add(afterHours);

        return album2;
    }

    public ArrayList<Album> getAlbum3(){
        ArrayList<Album> album3 = new ArrayList<>();

        album3.add(four);
        album3.add(takeMeHome);
        album3.add(upAllNight);

        return album3;
    }

    public ArrayList<Album> getAlbumNgot(){
        ArrayList<Album> album = new ArrayList<>();

        album.add(album2004);
        album.add(album5AM);
        album.add(gap);

        return album;
    }

    public ArrayList<Album> getAlbumCHH(){
        ArrayList<Album> album = new ArrayList<>();

        album.add(ngot);
        album.add(dot);
        album.add(thayChua);

        return album;
    }
    
    public ArrayList<Album> getAlbumTao(){
        ArrayList<Album> album = new ArrayList<>();

        album.add(album25);
        album.add(diathan);
        album.add(redRum);

        return album;
    }
    public ArrayList<Album> getAllAlbum(){
        ArrayList<Album> allAlbum = new ArrayList<>();

        allAlbum.add(myWorld);
        allAlbum.add(purpose);
        allAlbum.add(Changes);
        allAlbum.add(StartBoy);
        allAlbum.add(dawnFM);
        allAlbum.add(afterHours);
        allAlbum.add(four);
        allAlbum.add(takeMeHome);
        allAlbum.add(upAllNight);
        allAlbum.add(ngot);
        allAlbum.add(dot);
        allAlbum.add(thayChua);
        allAlbum.add(album2004);
        allAlbum.add(album5AM);
        allAlbum.add(gap);
        allAlbum.add(album25);
        allAlbum.add(diathan);
        allAlbum.add(redRum);

        return allAlbum;
    }

    public Album filterAlbum(@RequestParam(name="code", required = true) int id){
        ArrayList<Album> album = getAllAlbum();

        Album findAlbum = new Album();

        for (Album albumElement : album) {
            findAlbum = albumElement;
            break;
        }
        
        return findAlbum;
    }

}
